wget https://github.com/jgm/pandoc/releases/download/2.2.1/pandoc-2.2.1-1-amd64.deb

sudo dpkg -i pandoc-2.2.1-1-amd64.deb

pandoc --from=epub --to=asciidoc --extract-media=extracted-media microservices-antipatterns-and-pitfalls.epub > microservices-antipatterns-and-pitfalls.adoc

Mobi format
https://wiki.mobileread.com/wiki/MOBI
